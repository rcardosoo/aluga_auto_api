package com.aluga.auto.api.locacao.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aluga.auto.api.cliente.model.Cliente;
import com.aluga.auto.api.cliente.repository.IClienteRepository;
import com.aluga.auto.api.locacao.model.Locacao;
import com.aluga.auto.api.locacao.model.StatusLocacao;
import com.aluga.auto.api.locacao.repository.ILocacaoRepository;
import com.aluga.auto.api.util.UtilTests;
import com.aluga.auto.api.veiculo.model.Veiculo;
import com.aluga.auto.api.veiculo.repository.IVeiculoRepository;

public class LocacaoControllerTest extends UtilTests {
	
	@Autowired
	private ILocacaoRepository locacaoRepository;
	
	@Autowired
	private IVeiculoRepository veiculoRepository;
	
	@Autowired
	private IClienteRepository clienteRepository;
	
	private Veiculo veiculoForTest1;	
	private Cliente clienteForTest1;
	private Veiculo veiculoForTest2;
	private Veiculo veiculoForTest3;
	private Veiculo veiculoForTest4;
	private Cliente clienteForTest2;
	private Locacao locacaoForTest1;
	private Locacao locacaoForTest2;
	private Locacao locacaoForTest3;
	
	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();

		locacaoRepository.deleteAllInBatch();
		veiculoRepository.deleteAllInBatch();
		clienteRepository.deleteAllInBatch();
		clienteForTest1 = new Cliente("CT1", "111.222.333-45", "cli1@teste.com", "(83) 9999-9999");
		veiculoForTest1 = new Veiculo("AAA9537", "Gol", "Volkswagen", "Prateado", new BigDecimal(40.00));
		veiculoForTest1 = veiculoRepository.save(this.veiculoForTest1);
		clienteForTest1 = clienteRepository.save(this.clienteForTest1);
		
		clienteForTest2 = new Cliente("CT2", "990.222.333-45", "cli1@teste.com", "(83) 9999-9999");
		veiculoForTest2 = new Veiculo("OKA1234", "Polo", "Fiat", "Prateado", new BigDecimal(60.00));
		veiculoForTest2.setOcupado(true);
		veiculoForTest2 = veiculoRepository.save(this.veiculoForTest2);
		clienteForTest2 = clienteRepository.save(this.clienteForTest2);
		
		veiculoForTest3 = new Veiculo("KOE1711", "Uno", "Fiat", "Preto", new BigDecimal(45.00));
		veiculoForTest3.setOcupado(true);
		veiculoForTest3 = veiculoRepository.save(this.veiculoForTest3);
		
		veiculoForTest4 = new Veiculo("MMA9890", "Toro", "Fiat", "Branco", new BigDecimal(75.00));
		veiculoForTest4 = veiculoRepository.save(this.veiculoForTest3);
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, +1);
		locacaoForTest1 = new Locacao(clienteForTest2, veiculoForTest2, new Date(), c.getTime());
		locacaoForTest1.setStatus(StatusLocacao.ALUGADO.name());
		locacaoForTest1 = locacaoRepository.save(this.locacaoForTest1);

		c.add(Calendar.DATE, +2);
		locacaoForTest2 = new Locacao(clienteForTest2, veiculoForTest3, new Date(), c.getTime());
		c.add(Calendar.DATE, +4);
		locacaoForTest2.setDataLocacao(c.getTime());
		locacaoForTest2.setStatus(StatusLocacao.AGENDADO.name());
		locacaoForTest2 = locacaoRepository.save(this.locacaoForTest2);
		
		Calendar c2 = Calendar.getInstance();
		c.add(Calendar.DATE, -8);
		c2.add(Calendar.DATE, -4);
		locacaoForTest3 = new Locacao(clienteForTest2, veiculoForTest4, c.getTime(), c2.getTime());
		locacaoForTest3.setStatus(StatusLocacao.FINALIZADO.name());
		locacaoForTest3 = locacaoRepository.save(this.locacaoForTest3);
	}
	
	@Test
	public void alugarVeiculo() throws Exception {
		HashMap<String, String> map = new HashMap<>();
		map.put("dataEntrega", "31/10/2018 10:00:00");
		String jsonObject = json(map);
		
		String url = "/api/clientes/"+ clienteForTest1.getId() +"/veiculos/"+ veiculoForTest1.getId() +"/locacoes";
		
		this.mockMvc.perform(post(url)
			.contentType(contentType).content(jsonObject))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.cliente.nome", 	equalTo(clienteForTest1.getNome())))
			.andExpect(jsonPath("$.cliente.cpf", 	equalTo(clienteForTest1.getCpf())))
			.andExpect(jsonPath("$.veiculo.modelo", equalTo(veiculoForTest1.getModelo())))
			.andExpect(jsonPath("$.veiculo.placa", 	equalTo(veiculoForTest1.getPlaca())))
			.andExpect(jsonPath("$.status", 		equalTo(StatusLocacao.ALUGADO.name())));		
	}
	
	@Test
	public void entregarVeiculo() throws Exception {		
		String url = "/api/locacoes/"+locacaoForTest1.getId();
		
		this.mockMvc.perform(patch(url)
			.contentType(contentType))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.cliente.cpf", 	equalTo(clienteForTest2.getCpf())))
			.andExpect(jsonPath("$.veiculo.placa", 	equalTo(veiculoForTest2.getPlaca())))
			.andExpect(jsonPath("$.status", 		equalTo(StatusLocacao.FINALIZADO.name())));		
	}
	
	@Test
	public void cancelarReserva() throws Exception {		
		String url = "/api/reservas/"+locacaoForTest2.getId()+"/cancelamento";
		
		this.mockMvc.perform(patch(url)
			.contentType(contentType))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.cliente.cpf", 	equalTo(clienteForTest2.getCpf())))
			.andExpect(jsonPath("$.veiculo.placa", 	equalTo(veiculoForTest3.getPlaca())))
			.andExpect(jsonPath("$.status", 		equalTo(StatusLocacao.FINALIZADO.name())));		
	}
	
	@Test
	public void reservarVeiculo() throws Exception {		
		HashMap<String, String> map = new HashMap<>();
		map.put("dataLocacao", "10/11/2018 22:20:00");
		map.put("dataEntrega", "12/11/2018 15:00:00");
		
		String jsonObject = json(map);
		
		String url = "/api/clientes/"+ clienteForTest1.getId() +"/veiculos/"+ veiculoForTest1.getId() +"/reservas";
		
		this.mockMvc.perform(post(url)
			.contentType(contentType).content(jsonObject))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.cliente.nome", 	equalTo(clienteForTest1.getNome())))
			.andExpect(jsonPath("$.cliente.cpf", 	equalTo(clienteForTest1.getCpf())))
			.andExpect(jsonPath("$.veiculo.modelo", equalTo(veiculoForTest1.getModelo())))
			.andExpect(jsonPath("$.veiculo.placa", 	equalTo(veiculoForTest1.getPlaca())))
			.andExpect(jsonPath("$.status", 		equalTo(StatusLocacao.AGENDADO.name())));
	}

}
