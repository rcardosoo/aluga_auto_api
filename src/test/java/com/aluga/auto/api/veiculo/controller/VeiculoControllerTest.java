package com.aluga.auto.api.veiculo.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aluga.auto.api.cliente.repository.IClienteRepository;
import com.aluga.auto.api.locacao.repository.ILocacaoRepository;
import com.aluga.auto.api.util.UtilTests;
import com.aluga.auto.api.veiculo.model.Veiculo;
import com.aluga.auto.api.veiculo.repository.IVeiculoRepository;

public class VeiculoControllerTest extends UtilTests {
	private String baseControllerUrl = "/api/veiculos";

	@Autowired
	private ILocacaoRepository locacaoRepository;
	
	@Autowired
	private IVeiculoRepository veiculoRepository;
	
	@Autowired
	private IClienteRepository clienteRepository;
	
	private Veiculo veiculoForTest1;	
	private Veiculo veiculoForTest2;
	private Veiculo veiculoForTest3;
	
	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();

		locacaoRepository.deleteAllInBatch();
		veiculoRepository.deleteAllInBatch();
		clienteRepository.deleteAllInBatch();
		veiculoForTest1 = new Veiculo("AAA9537", "Gol", "Volkswagen", "Prateado", new BigDecimal(40.00));
		veiculoForTest2 = new Veiculo("OFF3657", "Etios", "Toyota", "Vermelho", new BigDecimal(50.00));
		veiculoForTest3 = new Veiculo("BCP4308", "Fit", "Honda", "Preto", new BigDecimal(38.00));
		veiculoForTest2 = veiculoRepository.save(this.veiculoForTest2);
		veiculoForTest3 = veiculoRepository.save(this.veiculoForTest3);
	}
	
	@Test
	public void cadastrarVeiculo() throws Exception {
		String jsonObject = json(this.veiculoForTest1);

		this.mockMvc.perform(post(baseControllerUrl)
			.contentType(contentType).content(jsonObject))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.placa", 	equalTo(veiculoForTest1.getPlaca())))
			.andExpect(jsonPath("$.modelo", equalTo(veiculoForTest1.getModelo())))
			.andExpect(jsonPath("$.marca", 	equalTo(veiculoForTest1.getMarca())))
			.andExpect(jsonPath("$.cor", 	equalTo(veiculoForTest1.getCor())));
	}

	@Test
	public void lerVeiculo() throws Exception {	
		this.mockMvc.perform(get(baseControllerUrl + "/" + veiculoForTest2.getId()))
			.andExpect(content().contentType(contentType))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.placa", 	equalTo(veiculoForTest2.getPlaca())))
			.andExpect(jsonPath("$.modelo", equalTo(veiculoForTest2.getModelo())))
			.andExpect(jsonPath("$.marca", 	equalTo(veiculoForTest2.getMarca())))
			.andExpect(jsonPath("$.cor", 	equalTo(veiculoForTest2.getCor())));
	}

	@Test
	public void listarTodosOsVeiculos() throws Exception {
		this.mockMvc.perform(get(baseControllerUrl))
			.andExpect(content().contentType(contentType))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", notNullValue()));
	}

	@Test
	public void excluirVeiculo() throws Exception {
		this.mockMvc.perform(delete(baseControllerUrl + "/" + veiculoForTest3.getId())
			.contentType(contentType))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.placa", 	equalTo(veiculoForTest3.getPlaca())))
			.andExpect(jsonPath("$.modelo", equalTo(veiculoForTest3.getModelo())))
			.andExpect(jsonPath("$.marca", 	equalTo(veiculoForTest3.getMarca())))
			.andExpect(jsonPath("$.cor", 	equalTo(veiculoForTest3.getCor())));
	}

	@Test
	public void atualizarVeiculo() throws Exception {
		veiculoForTest2.setCor("Branco");

		String jsonObject = json(veiculoForTest2);

		this.mockMvc.perform(put(baseControllerUrl + "/" + veiculoForTest2.getId())
			.contentType(contentType)
			.content(jsonObject))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.placa", 	equalTo(veiculoForTest2.getPlaca())))
			.andExpect(jsonPath("$.modelo", equalTo(veiculoForTest2.getModelo())))
			.andExpect(jsonPath("$.marca", 	equalTo(veiculoForTest2.getMarca())))
			.andExpect(jsonPath("$.cor", 	equalTo(veiculoForTest2.getCor())));
	}
}
