package com.aluga.auto.api.cliente.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aluga.auto.api.cliente.model.Cliente;
import com.aluga.auto.api.cliente.repository.IClienteRepository;
import com.aluga.auto.api.locacao.repository.ILocacaoRepository;
import com.aluga.auto.api.util.UtilTests;
import com.aluga.auto.api.veiculo.repository.IVeiculoRepository;

public class ClienteControllerTest extends UtilTests {
	private String baseControllerUrl = "/api/clientes";

	@Autowired
	private ILocacaoRepository locacaoRepository;
	
	@Autowired
	private IVeiculoRepository veiculoRepository;
	
	@Autowired
	private IClienteRepository clienteRepository;

	private Cliente clienteForTest1;	
	private Cliente clienteForTest2;
	private Cliente clienteForTest3;

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		
		locacaoRepository.deleteAllInBatch();
		veiculoRepository.deleteAllInBatch();
		clienteRepository.deleteAllInBatch();
		clienteForTest1 = new Cliente("CT1", "111.222.333-45", "cli1@teste.com", "(83) 9999-9999");
		clienteForTest2 = new Cliente("CT2", "999.222.333-45", "cli2@teste.com", "(83) 8888-9999");
		clienteForTest3 = new Cliente("CT3", "888.333.444-45", "cli3@teste.com", "(83) 7777-9999");
		clienteForTest2 = clienteRepository.save(this.clienteForTest2);
		clienteForTest3 = clienteRepository.save(this.clienteForTest3);
	}

	@Test
	public void cadastrarCliente() throws Exception {
		String jsonObject = json(this.clienteForTest1);

		this.mockMvc.perform(post(baseControllerUrl)
			.contentType(contentType).content(jsonObject))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.nome", 		equalTo(clienteForTest1.getNome())))
			.andExpect(jsonPath("$.cpf", 		equalTo(clienteForTest1.getCpf())))
			.andExpect(jsonPath("$.email", 		equalTo(clienteForTest1.getEmail())))
			.andExpect(jsonPath("$.telefone", 	equalTo(clienteForTest1.getTelefone())));
	}

	@Test
	public void lerCliente() throws Exception {	
		this.mockMvc.perform(get(baseControllerUrl + "/" + clienteForTest2.getId()))
			.andExpect(content().contentType(contentType))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.nome", 		equalTo(clienteForTest2.getNome())))
			.andExpect(jsonPath("$.cpf", 		equalTo(clienteForTest2.getCpf())))
			.andExpect(jsonPath("$.email", 		equalTo(clienteForTest2.getEmail())))
			.andExpect(jsonPath("$.telefone", 	equalTo(clienteForTest2.getTelefone())));
	}

	@Test
	public void listarTodosOsCliente() throws Exception {
		this.mockMvc.perform(get(baseControllerUrl))
			.andExpect(content().contentType(contentType))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", notNullValue()));
	}

	@Test
	public void excluirCliente() throws Exception {
		this.mockMvc.perform(delete(baseControllerUrl + "/" + clienteForTest3.getId())
			.contentType(contentType))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.nome", 		equalTo(clienteForTest3.getNome())))
			.andExpect(jsonPath("$.cpf", 		equalTo(clienteForTest3.getCpf())))
			.andExpect(jsonPath("$.email", 		equalTo(clienteForTest3.getEmail())))
			.andExpect(jsonPath("$.telefone", 	equalTo(clienteForTest3.getTelefone())));
	}

	@Test
	public void atualizarCliente() throws Exception {
		clienteForTest2.setEmail("novo@email.com");
		clienteForTest2.setTelefone("(83) 9 55775989");

		String jsonObject = json(clienteForTest2);

		this.mockMvc.perform(put(baseControllerUrl + "/" + clienteForTest2.getId())
			.contentType(contentType)
			.content(jsonObject))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.nome", 		equalTo(clienteForTest2.getNome())))
			.andExpect(jsonPath("$.cpf", 		equalTo(clienteForTest2.getCpf())))
			.andExpect(jsonPath("$.email", 		equalTo(clienteForTest2.getEmail())))
			.andExpect(jsonPath("$.telefone", 	equalTo(clienteForTest2.getTelefone())));
	}

}
