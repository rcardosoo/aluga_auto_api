package com.aluga.auto.api.relatorio.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aluga.auto.api.cliente.model.Cliente;
import com.aluga.auto.api.cliente.repository.IClienteRepository;
import com.aluga.auto.api.locacao.model.Locacao;
import com.aluga.auto.api.locacao.model.StatusLocacao;
import com.aluga.auto.api.locacao.repository.ILocacaoRepository;
import com.aluga.auto.api.util.UtilTests;
import com.aluga.auto.api.veiculo.model.Veiculo;
import com.aluga.auto.api.veiculo.repository.IVeiculoRepository;

public class RelatorioControllerTest extends UtilTests {
	
	@Autowired
	private ILocacaoRepository locacaoRepository;
	
	@Autowired
	private IVeiculoRepository veiculoRepository;
	
	@Autowired
	private IClienteRepository clienteRepository;
	
	private Veiculo veiculoForTest1;	
	private Cliente clienteForTest1;
	private Locacao locacaoForTest1;
	private Locacao locacaoForTest2;
	private Veiculo veiculoForTest2;
	private Locacao locacaoForTest3;
	private Veiculo veiculoForTest3;
	
	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();

		locacaoRepository.deleteAllInBatch();
		veiculoRepository.deleteAllInBatch();
		clienteRepository.deleteAllInBatch();
		clienteForTest1 = new Cliente("CT1", "111.222.333-45", "cli1@teste.com", "(83) 9999-9999");
		clienteForTest1 = clienteRepository.save(this.clienteForTest1);

		veiculoForTest1 = new Veiculo("IKU8874", "Gol", "Volkswagen", "Prateado", new BigDecimal(40.00));
		veiculoForTest1 = veiculoRepository.save(this.veiculoForTest1);
		
		veiculoForTest2 = new Veiculo("TEH2233", "Gol", "Volkswagen", "Prateado", new BigDecimal(40.00));
		veiculoForTest2 = veiculoRepository.save(this.veiculoForTest2);
		
		veiculoForTest3 = new Veiculo("BBG6787", "Gol", "Volkswagen", "Prateado", new BigDecimal(40.00));
		veiculoForTest3 = veiculoRepository.save(this.veiculoForTest3);
		
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		locacaoForTest1 = new Locacao(clienteForTest1, veiculoForTest1, c1.getTime(), c2.getTime());
		locacaoForTest1.setDataFinalizacao(c2.getTime());
		locacaoForTest1.setStatus(StatusLocacao.FINALIZADO.name());
		locacaoForTest1 = locacaoRepository.save(this.locacaoForTest1);
		
		c1.add(Calendar.MONTH, -1);
		c2.add(Calendar.MONTH, -1);
		locacaoForTest2 = new Locacao(clienteForTest1, veiculoForTest2, c1.getTime(), c2.getTime());
		locacaoForTest2.setDataFinalizacao(c2.getTime());
		locacaoForTest2.setStatus(StatusLocacao.FINALIZADO.name());
		locacaoForTest2 = locacaoRepository.save(this.locacaoForTest1);
		
		c1.add(Calendar.MONTH, -6);
		c2.add(Calendar.MONTH, -5);
		locacaoForTest3 = new Locacao(clienteForTest1, veiculoForTest3, c1.getTime(), c2.getTime());
		locacaoForTest3.setDataFinalizacao(c2.getTime());
		locacaoForTest3.setStatus(StatusLocacao.FINALIZADO.name());
		locacaoForTest3 = locacaoRepository.save(this.locacaoForTest1);

	}

	@Test
	public void gerarRelatorioDiario() throws Exception {				
		this.mockMvc.perform(get("/api/relatorios?filtro=dia")
			.contentType(contentType))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.locacoes",  not(new ArrayList<>())));
	}
	
	@Test
	public void gerarRelatorioMensal() throws Exception {				
		this.mockMvc.perform(get("/api/relatorios?filtro=mes")
			.contentType(contentType))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.locacoes",  not(new ArrayList<>())));
	}
	
	@Test
	public void gerarRelatorioTotal() throws Exception {				
		this.mockMvc.perform(get("/api/relatorios")
			.contentType(contentType))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.locacoes", not(new ArrayList<>())));
	}

}
