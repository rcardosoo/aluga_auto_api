package com.aluga.auto.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlugaAutoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlugaAutoApiApplication.class, args);
	}
}
