package com.aluga.auto.api.locacao.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.aluga.auto.api.cliente.model.Cliente;
import com.aluga.auto.api.veiculo.model.Veiculo;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tb_locacao")
@NoArgsConstructor
public class Locacao implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter @Setter
	private Long id;
	
	@Getter @Setter
	@NotNull
	@ManyToOne
    @JoinColumn(name = "cliente_id")
	private Cliente cliente;
	
	@Getter @Setter
	@NotNull
	@ManyToOne
    @JoinColumn(name = "veiculo_id")
	private Veiculo veiculo;
	
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
	@Getter @Setter
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", timezone="America/Sao_Paulo")
    private Date dataLocacao;
    
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
	@Getter @Setter
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", timezone="America/Sao_Paulo")
    private Date dataEntregaPrevista;
    
    @Temporal(TemporalType.TIMESTAMP)
	@Getter @Setter
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", timezone="America/Sao_Paulo")
    private Date dataFinalizacao;
    
    @NotNull
	@Getter @Setter
    private String status;

	public Locacao(Cliente cliente, Veiculo veiculo, Date dataLocacao, Date dataEntrega) {
		super();
		this.cliente = cliente;
		this.veiculo = veiculo;
		this.dataLocacao = dataLocacao;
		this.dataEntregaPrevista = dataEntrega;
		this.status = StatusLocacao.ABERTO.name();
	}
	
}
