package com.aluga.auto.api.locacao.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aluga.auto.api.cliente.model.Cliente;
import com.aluga.auto.api.cliente.repository.IClienteRepository;
import com.aluga.auto.api.locacao.model.Locacao;
import com.aluga.auto.api.locacao.model.StatusLocacao;
import com.aluga.auto.api.locacao.repository.ILocacaoRepository;
import com.aluga.auto.api.locacao.service.ILocacaoService;
import com.aluga.auto.api.veiculo.model.Veiculo;
import com.aluga.auto.api.veiculo.repository.IVeiculoRepository;

@Service
public class LocacaoImediataServiceImpl implements ILocacaoService {

	@Autowired
	private IClienteRepository clienteRepository;
	
	@Autowired
	private IVeiculoRepository veiculoRepository;
	
	@Autowired
	private ILocacaoRepository locacaoRepository;
	
	@Override
	public Locacao alugarVeiculo(Locacao locacao) {
		locacao.setDataLocacao(new Date());
		locacao.getVeiculo().setOcupado(true);
		locacao.setStatus(StatusLocacao.ALUGADO.name());
		veiculoRepository.save(locacao.getVeiculo());
		return locacaoRepository.save(locacao);
	}

	@Override
	public Locacao validarRequestLocacao(Long clienteId, Long veiculoId) {	
		Optional<Cliente> cliente = clienteRepository.findById(clienteId);
		Optional<Veiculo> veiculo = veiculoRepository.findById(veiculoId);
		
		if (cliente.isPresent() && veiculo.isPresent()) {
			Locacao locacao = new Locacao();
			locacao.setCliente(cliente.get());
			locacao.setVeiculo(veiculo.get());
			return locacao;
		}
			
		return null;
	}

	@Override
	public Date converterData(String dataEntrega) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    	
		JSONObject dataJSON = new JSONObject(dataEntrega);
		String dataValue = dataJSON.getString("dataEntrega");
    	
		return formatter.parse(dataValue);	
    }

	@Override
	public Locacao entregarVeiculo(Long locacaoId) {
		Optional<Locacao> locacaoOpt = locacaoRepository.findById(locacaoId);
		
		if (locacaoOpt.isPresent()) {
			Locacao locacao = locacaoOpt.get();
			if (locacao.getStatus().equals(StatusLocacao.ALUGADO.name())) {
				locacao.getVeiculo().setOcupado(false);
				veiculoRepository.save(locacao.getVeiculo());
				locacao.setStatus(StatusLocacao.FINALIZADO.name());
				locacao.setDataFinalizacao(new Date());
				return locacaoRepository.save(locacao);
			}
			throw new IllegalArgumentException();
		}
		return null;
	}
	
}
