package com.aluga.auto.api.locacao.service;

import java.text.ParseException;

import com.aluga.auto.api.locacao.model.Locacao;

public interface IReservaService {
	
	public Locacao reservarVeiculo(Locacao locacao);
	
	public Locacao converterDatas(String datas, Locacao locacao) throws ParseException;

	public Locacao cancelarReserva(Long locacaoId);
}
