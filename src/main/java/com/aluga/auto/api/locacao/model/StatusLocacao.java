package com.aluga.auto.api.locacao.model;

public enum StatusLocacao {
	ALUGADO,
	FINALIZADO,
	AGENDADO,
	ABERTO;
}
