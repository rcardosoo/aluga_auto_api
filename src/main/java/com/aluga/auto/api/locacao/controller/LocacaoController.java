package com.aluga.auto.api.locacao.controller;

import java.text.ParseException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aluga.auto.api.exception.ResourceNotFoundException;
import com.aluga.auto.api.locacao.model.Locacao;
import com.aluga.auto.api.locacao.service.ILocacaoService;
import com.aluga.auto.api.locacao.service.IReservaService;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
@SuppressWarnings("rawtypes")
public class LocacaoController {

	@Autowired
	private ILocacaoService locacaoService;
	
	@Autowired
	private IReservaService reservaService;

	@PostMapping(value = "/clientes/{clienteId}/veiculos/{veiculoId}/locacoes")
	public ResponseEntity alugarVeiculo(@PathVariable Long clienteId, @PathVariable Long veiculoId, @Valid @RequestBody String dataEntrega) {
		Locacao locacao = locacaoService.validarRequestLocacao(clienteId, veiculoId);

		if (locacao != null) {
			try {
				locacao.setDataEntregaPrevista(locacaoService.converterData(dataEntrega));
			} catch (ParseException e) {
				throw new IllegalArgumentException();
			}

			if (locacao.getVeiculo().getOcupado()) 
				throw new IllegalArgumentException();

			return ResponseEntity.status(HttpStatus.CREATED).body(locacaoService.alugarVeiculo(locacao));
		}

		throw new ResourceNotFoundException();
	}
	
	@PatchMapping(value = "/locacoes/{locacaoId}")
	public ResponseEntity entregarVeiculo(@PathVariable Long locacaoId) {	
		Locacao locacaoFinalizada = locacaoService.entregarVeiculo(locacaoId);
		
		if (locacaoFinalizada != null) {
			return ResponseEntity.ok(locacaoFinalizada);
		}
		
		throw new ResourceNotFoundException(locacaoId);
	}
	
	@PatchMapping(value = "/locacoes/{locacaoId}/cancelamento")
	public ResponseEntity cancelarReserva(@PathVariable Long locacaoId) {	
		Locacao locacaoFinalizada = reservaService.cancelarReserva(locacaoId);
		
		if (locacaoFinalizada != null) {
			return ResponseEntity.ok(locacaoFinalizada);
		}
		
		throw new ResourceNotFoundException(locacaoId);
	}
	
	@PostMapping(value = "/clientes/{clienteId}/veiculos/{veiculoId}/reservas")
	public ResponseEntity reservarVeiculo(@PathVariable Long clienteId, @PathVariable Long veiculoId, @Valid @RequestBody String datas) {
		Locacao locacao = locacaoService.validarRequestLocacao(clienteId, veiculoId);

		if (locacao != null) {
			try {
				locacao = reservaService.converterDatas(datas, locacao);
			} catch (ParseException e) {
				throw new IllegalArgumentException();
			}

			if (locacao.getVeiculo().getOcupado()) 
				throw new IllegalArgumentException();

			return ResponseEntity.status(HttpStatus.CREATED).body(reservaService.reservarVeiculo(locacao));
		}

		throw new ResourceNotFoundException();
	}
}
