package com.aluga.auto.api.locacao.service;

import java.text.ParseException;
import java.util.Date;

import com.aluga.auto.api.locacao.model.Locacao;

public interface ILocacaoService {
	
	public Locacao alugarVeiculo(Locacao locacao);
	
	public Locacao validarRequestLocacao(Long clienteId, Long veiculoId);
	
	public Date converterData(String dataEntrega) throws ParseException;

	public Locacao entregarVeiculo(Long locacaoId);
	
}
