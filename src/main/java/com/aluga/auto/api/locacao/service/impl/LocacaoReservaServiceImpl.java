package com.aluga.auto.api.locacao.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.Timer;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aluga.auto.api.locacao.model.Locacao;
import com.aluga.auto.api.locacao.model.StatusLocacao;
import com.aluga.auto.api.locacao.repository.ILocacaoRepository;
import com.aluga.auto.api.locacao.service.IReservaService;
import com.aluga.auto.api.veiculo.repository.IVeiculoRepository;

@Service
public class LocacaoReservaServiceImpl implements IReservaService {

	@Autowired
	private IVeiculoRepository veiculoRepository;

	@Autowired
	private ILocacaoRepository locacaoRepository;

	@Override
	public Locacao reservarVeiculo(Locacao locacao) {
		locacao.getVeiculo().setOcupado(true);
		locacao.setStatus(StatusLocacao.AGENDADO.name());
		veiculoRepository.save(locacao.getVeiculo());

		long periodo = 60000;
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new AgendamentoLocacaoTimer(locacao, locacaoRepository), 0, periodo);

		return locacaoRepository.save(locacao);
	}

	@Override
	public Locacao converterDatas(String datas, Locacao locacao) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		JSONObject dataJSON = new JSONObject(datas);
		String dataValueLocacao = dataJSON.getString("dataLocacao");
		String dataValueEntrega = dataJSON.getString("dataEntrega");

		locacao.setDataLocacao(formatter.parse(dataValueLocacao));
		locacao.setDataEntregaPrevista(formatter.parse(dataValueEntrega));	

		return locacao;
	}

	@Override
	public Locacao cancelarReserva(Long locacaoId) {
		Optional<Locacao> locacaoOpt = locacaoRepository.findById(locacaoId);

		if (locacaoOpt.isPresent()) {
			Locacao locacao = locacaoOpt.get();
			if (locacao.getStatus().equals(StatusLocacao.AGENDADO.name())) {
				locacao.getVeiculo().setOcupado(false);
				veiculoRepository.save(locacao.getVeiculo());
				locacao.setStatus(StatusLocacao.FINALIZADO.name());
				locacao.setDataFinalizacao(new Date());
				return locacaoRepository.save(locacao);
			}
			throw new IllegalArgumentException();
		}
		return null;
	}

}
