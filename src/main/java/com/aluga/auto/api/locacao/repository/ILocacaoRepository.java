package com.aluga.auto.api.locacao.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aluga.auto.api.locacao.model.Locacao;

@Repository
public interface ILocacaoRepository extends JpaRepository<Locacao, Long> {
	
	public List<Locacao> findByStatusAndDataFinalizacaoBetween(String status, Date dataInicio, Date dataFim);
	
	public List<Locacao> findByStatus(String status);
}
