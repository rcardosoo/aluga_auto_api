package com.aluga.auto.api.locacao.service.impl;

import java.util.Date;
import java.util.TimerTask;

import com.aluga.auto.api.locacao.model.Locacao;
import com.aluga.auto.api.locacao.model.StatusLocacao;
import com.aluga.auto.api.locacao.repository.ILocacaoRepository;

public class AgendamentoLocacaoTimer extends TimerTask {
	private Locacao locacao;
	
	private ILocacaoRepository locacaoRepository;
	
	public AgendamentoLocacaoTimer(Locacao locacao, ILocacaoRepository locacaoRepository) {
		super();
		this.locacao = locacao;
		this.locacaoRepository = locacaoRepository;
	}

	@Override
	public void run() {
				
		if (locacao.getStatus().equals(StatusLocacao.FINALIZADO.name())) {
			cancel();
		}
				
		if (locacao.getDataLocacao().before(new Date())) {
			locacao.setStatus(StatusLocacao.ALUGADO.name());
			locacaoRepository.save(locacao);
			cancel();
		}
	}
	
}
