package com.aluga.auto.api.veiculo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aluga.auto.api.veiculo.model.Veiculo;

@Repository
public interface IVeiculoRepository extends JpaRepository<Veiculo, Long> {

}
