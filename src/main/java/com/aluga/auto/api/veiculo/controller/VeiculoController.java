package com.aluga.auto.api.veiculo.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aluga.auto.api.exception.ResourceNotFoundException;
import com.aluga.auto.api.veiculo.model.Veiculo;
import com.aluga.auto.api.veiculo.service.IVeiculoService;

@RestController
@RequestMapping(value = "/api/veiculos", produces = MediaType.APPLICATION_JSON_VALUE)
@SuppressWarnings("rawtypes")
public class VeiculoController {

	@Autowired
	private IVeiculoService veiculoService;
	
	@PostMapping()
	public ResponseEntity cadastrarVeiculo(@Valid @RequestBody Veiculo veiculo) {
		return ResponseEntity.status(HttpStatus.CREATED).body(veiculoService.cadastrarVeiculo(veiculo));
	}
	
	@GetMapping(value = "/{veiculoId}")
	public ResponseEntity lerVeiculo(@PathVariable("veiculoId") Long veiculoId) {
		Optional<Veiculo> veiculo = veiculoService.lerVeiculo(veiculoId);
		
		if (veiculo.isPresent()) {
			return ResponseEntity.ok(veiculo.get());
		}
		
		throw new ResourceNotFoundException(veiculoId);
	}
	
	@GetMapping()
	public ResponseEntity<List<Veiculo>> listarVeiculos() {
		return ResponseEntity.ok(veiculoService.listarVeiculos());
	}
	
	@PutMapping(value = "/{veiculoId}")
	public ResponseEntity atualizarVeiculo(@PathVariable("veiculoId") Long veiculoId, @Valid @RequestBody Veiculo veiculo) {
		Veiculo atualizado = veiculoService.atualizarVeiculo(veiculoId, veiculo);
		
		if (atualizado != null) {
			return ResponseEntity.ok(atualizado);
		}
		
		throw new ResourceNotFoundException(veiculoId);
	}
	
	@DeleteMapping(value = "/{veiculoId}")
	public ResponseEntity excluirVeiculo(@PathVariable("veiculoId") Long veiculoId) {
		Veiculo deletado = veiculoService.excluirVeiculo(veiculoId);
		
		if (deletado != null) {
			return ResponseEntity.ok(deletado);
		}
		
		throw new ResourceNotFoundException(veiculoId);
	}
}
