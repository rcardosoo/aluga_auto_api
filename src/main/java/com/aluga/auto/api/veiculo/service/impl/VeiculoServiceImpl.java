package com.aluga.auto.api.veiculo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aluga.auto.api.veiculo.model.Veiculo;
import com.aluga.auto.api.veiculo.repository.IVeiculoRepository;
import com.aluga.auto.api.veiculo.service.IVeiculoService;

@Service
public class VeiculoServiceImpl implements IVeiculoService {
	
	@Autowired
	private IVeiculoRepository veiculoRepository;
	
	@Override
	public Veiculo cadastrarVeiculo(Veiculo veiculo) {
		return veiculoRepository.save(veiculo);
	}

	@Override
	public Optional<Veiculo> lerVeiculo(Long veiculoId) {
		return veiculoRepository.findById(veiculoId);
	}

	@Override
	public List<Veiculo> listarVeiculos() {
		return veiculoRepository.findAll();
	}

	@Override
	public Veiculo atualizarVeiculo(Long veiculoId, Veiculo novoVeiculo) {
		return veiculoRepository.findById(veiculoId).map(veiculo -> {
			novoVeiculo.setId(veiculoId);
			return veiculoRepository.save(novoVeiculo);
		}).orElse(null);
	}

	@Override
	public Veiculo excluirVeiculo(Long veiculoId) {
		return veiculoRepository.findById(veiculoId).map(veiculo -> {
			veiculoRepository.deleteById(veiculoId);
			return veiculo;
		}).orElse(null);
	}
	
}
