package com.aluga.auto.api.veiculo.service;

import java.util.List;
import java.util.Optional;

import com.aluga.auto.api.veiculo.model.Veiculo;

public interface IVeiculoService {
	
	public Veiculo cadastrarVeiculo(Veiculo veiculo);

	public Optional<Veiculo> lerVeiculo(Long veiculoId);

	public List<Veiculo> listarVeiculos();
	
	public Veiculo atualizarVeiculo(Long veiculoId, Veiculo novoVeiculo);
	
	public Veiculo excluirVeiculo(Long veiculoId);
}
