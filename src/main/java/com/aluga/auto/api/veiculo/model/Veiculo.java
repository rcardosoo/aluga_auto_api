package com.aluga.auto.api.veiculo.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tb_veiculo", 
uniqueConstraints=@UniqueConstraint(columnNames={"placa"}))
@NoArgsConstructor
public class Veiculo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter @Setter
	private Long id;
	
	@Size(max = 7)
	@NotNull
	@Getter @Setter
	private String placa;
	
	@Size(max = 50)
	@NotNull
	@Getter @Setter
	private String modelo;
	
	@Size(max = 50)
	@NotNull
	@Getter @Setter
	private String marca;
	
	@Size(max = 50)
	@NotNull
	@Getter @Setter
	private String cor;
	
	@Getter @Setter
	@NotNull
	private BigDecimal valorPorDia;
	  
	@NotNull
	@Getter @Setter
	private Boolean ocupado = false;

	public Veiculo(String placa, String modelo, String marca, String cor, BigDecimal valorPorDia) {
		super();
		this.placa = placa;
		this.modelo = modelo;
		this.marca = marca;
		this.cor = cor;
		this.valorPorDia = valorPorDia;
	}
	
}
