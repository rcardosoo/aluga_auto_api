package com.aluga.auto.api.relatorio.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aluga.auto.api.locacao.model.Locacao;
import com.aluga.auto.api.locacao.repository.ILocacaoRepository;
import com.aluga.auto.api.relatorio.model.Relatorio;
import com.aluga.auto.api.relatorio.service.IRelatorioService;
import com.aluga.auto.api.relatorio.strategy.ICalculoValorStrategy;
import com.aluga.auto.api.relatorio.strategy.concrete.CalculoDiarioConcreteStrategy;
import com.aluga.auto.api.relatorio.strategy.concrete.CalculoMensalConcreteStrategy;
import com.aluga.auto.api.relatorio.strategy.concrete.CalculoTotalConcreteStrategy;

@Service
public class RelatorioServiceImpl implements IRelatorioService {
	
	private ICalculoValorStrategy calculoStrategy;
	
	@Autowired
	private ILocacaoRepository locacaoRepository;
	
	@Override
	public HashMap<String, Object> gerarRelatorio(String filtro) {
		this.calculoStrategy = factoryStrategy(filtro);
		List<Locacao> locacoes = calculoStrategy.obterLocacoes();	
		return this.montarRelatorio(locacoes);
	}
	
	@Override
	public HashMap<String, Object> montarRelatorio(List<Locacao> locacoes) {
		HashMap<String, Object> relatorioMap = new HashMap<>();
		List<Relatorio> listaLocacoes = new ArrayList<>();
		BigDecimal valorAcumulado = new BigDecimal(0.0); 
		
		if (!locacoes.isEmpty()) {
			for (Locacao locacao : locacoes) {
				Relatorio currentLocacao = new Relatorio();
				currentLocacao.setDataLocacao(locacao.getDataLocacao());
				currentLocacao.setDataFinalizacao(locacao.getDataFinalizacao());
				currentLocacao.setValorLocacao(locacao.getVeiculo().getValorPorDia());
				valorAcumulado.add(valorAcumulado.add(this.calcularValorLocacao(locacao)));
				listaLocacoes.add(currentLocacao);
			}	
		}
		relatorioMap.put("valorTotalDoPeriodo", valorAcumulado);
		relatorioMap.put("locacoes", listaLocacoes);
		return relatorioMap;
	}
	
	@Override
	public BigDecimal calcularValorLocacao(Locacao locacao) {
		if (locacao.getDataLocacao() == null || locacao.getDataFinalizacao() == null) {
			return new BigDecimal(0.0);
		}
		
		long diferenca = (locacao.getDataLocacao().getTime() - locacao.getDataFinalizacao().getTime()) + 3600000;
		double dias = diferenca / 86400000L;
		return locacao.getVeiculo().getValorPorDia().multiply(new BigDecimal(dias));
	}
	
	public ICalculoValorStrategy factoryStrategy(String filtro) {
		
		if (filtro == null) {
			return new CalculoTotalConcreteStrategy(locacaoRepository);
		}
				
		if (filtro.equals("dia")) {
			return new CalculoDiarioConcreteStrategy(locacaoRepository);
		} else if (filtro.equals("mes")) {
			return new CalculoMensalConcreteStrategy(locacaoRepository);
		} else if (filtro.equals("total")) {
			return new CalculoTotalConcreteStrategy(locacaoRepository);
		} else {
			throw new IllegalArgumentException();
		}
	}

}
