package com.aluga.auto.api.relatorio.strategy.concrete;

import java.util.Calendar;
import java.util.List;

import com.aluga.auto.api.locacao.model.Locacao;
import com.aluga.auto.api.locacao.model.StatusLocacao;
import com.aluga.auto.api.locacao.repository.ILocacaoRepository;
import com.aluga.auto.api.relatorio.strategy.ICalculoValorStrategy;

public class CalculoMensalConcreteStrategy implements ICalculoValorStrategy {
	private ILocacaoRepository locacaoRepository;
	
	public CalculoMensalConcreteStrategy(ILocacaoRepository locacaoRepository) {
		super();
		this.locacaoRepository = locacaoRepository;
	}
	
	@Override
	public List<Locacao> obterLocacoes() {
		Calendar dataInicio = Calendar.getInstance();
		dataInicio.add(Calendar.MONTH, -1);
		dataInicio.set(Calendar.DATE, 1);
		
		Calendar dataFim = Calendar.getInstance();
		dataInicio.set(Calendar.DATE, 1);
		dataInicio.add(Calendar.DATE, -1);
		
		return locacaoRepository
				.findByStatusAndDataFinalizacaoBetween(StatusLocacao.FINALIZADO.name(), dataInicio.getTime(), dataFim.getTime());
	}

}
