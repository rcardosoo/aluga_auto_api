package com.aluga.auto.api.relatorio.strategy;

import java.util.List;

import com.aluga.auto.api.locacao.model.Locacao;

public interface ICalculoValorStrategy {
	public List<Locacao> obterLocacoes();
}
