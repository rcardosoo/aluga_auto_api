package com.aluga.auto.api.relatorio.strategy.concrete;

import java.util.List;

import com.aluga.auto.api.locacao.model.Locacao;
import com.aluga.auto.api.locacao.model.StatusLocacao;
import com.aluga.auto.api.locacao.repository.ILocacaoRepository;
import com.aluga.auto.api.relatorio.strategy.ICalculoValorStrategy;

public class CalculoTotalConcreteStrategy implements ICalculoValorStrategy {
	private ILocacaoRepository locacaoRepository;
	
	public CalculoTotalConcreteStrategy(ILocacaoRepository locacaoRepository) {
		super();
		this.locacaoRepository = locacaoRepository;
	}
	
	@Override
	public List<Locacao> obterLocacoes() {
		return locacaoRepository.findByStatus(StatusLocacao.FINALIZADO.name());
	}

}
