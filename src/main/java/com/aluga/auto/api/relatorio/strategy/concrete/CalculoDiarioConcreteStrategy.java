package com.aluga.auto.api.relatorio.strategy.concrete;

import java.util.Calendar;
import java.util.List;

import com.aluga.auto.api.locacao.model.Locacao;
import com.aluga.auto.api.locacao.model.StatusLocacao;
import com.aluga.auto.api.locacao.repository.ILocacaoRepository;
import com.aluga.auto.api.relatorio.strategy.ICalculoValorStrategy;

public class CalculoDiarioConcreteStrategy implements ICalculoValorStrategy {
	private ILocacaoRepository locacaoRepository;
	
	public CalculoDiarioConcreteStrategy(ILocacaoRepository locacaoRepository) {
		super();
		this.locacaoRepository = locacaoRepository;
	}

	@Override
	public List<Locacao> obterLocacoes() {		
		Calendar dataInicio = Calendar.getInstance();
		dataInicio.set(Calendar.HOUR_OF_DAY, 0);
		
		Calendar dataFim = Calendar.getInstance();
		dataFim.set(Calendar.HOUR_OF_DAY, 23);
		
		return locacaoRepository
				.findByStatusAndDataFinalizacaoBetween(StatusLocacao.FINALIZADO.name(), dataInicio.getTime(), dataFim.getTime());
	}

}
