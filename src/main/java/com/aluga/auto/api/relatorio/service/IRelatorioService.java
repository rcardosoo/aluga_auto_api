package com.aluga.auto.api.relatorio.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import com.aluga.auto.api.locacao.model.Locacao;

public interface IRelatorioService {
	public HashMap<String, Object> gerarRelatorio(String filtro);

	public HashMap<String, Object> montarRelatorio(List<Locacao> locacoes);

	public BigDecimal calcularValorLocacao(Locacao locacao);
}
