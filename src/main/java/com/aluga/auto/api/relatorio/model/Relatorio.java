package com.aluga.auto.api.relatorio.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Relatorio implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", timezone="America/Sao_Paulo")
    private Date dataLocacao;
    
	@Getter @Setter
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", timezone="America/Sao_Paulo")
    private Date dataFinalizacao;
	
	@Getter @Setter
	private BigDecimal valorLocacao;
	
}
