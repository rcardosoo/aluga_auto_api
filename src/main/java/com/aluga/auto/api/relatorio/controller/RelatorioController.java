package com.aluga.auto.api.relatorio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aluga.auto.api.relatorio.service.IRelatorioService;

@RestController
@RequestMapping(value = "/api/relatorios", produces = MediaType.APPLICATION_JSON_VALUE)
@SuppressWarnings("rawtypes")
public class RelatorioController {

	@Autowired
	private IRelatorioService relatorioService;
	
	@GetMapping()
	public ResponseEntity gerarRelatorio(@RequestParam(value = "filtro", required = false) String filtro) {
		return ResponseEntity.ok(relatorioService.gerarRelatorio(filtro));
	}
}
