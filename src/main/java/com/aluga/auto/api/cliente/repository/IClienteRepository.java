package com.aluga.auto.api.cliente.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aluga.auto.api.cliente.model.Cliente;

@Repository
public interface IClienteRepository extends JpaRepository<Cliente, Long> {
	public Cliente findByCpf(String cpf);
}
