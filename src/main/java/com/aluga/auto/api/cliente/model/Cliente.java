package com.aluga.auto.api.cliente.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tb_cliente", 
uniqueConstraints=@UniqueConstraint(columnNames={"cpf"}))
@NoArgsConstructor
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter @Setter
	private Long id;

	@Size(max = 100)
	@NotNull
	@Getter @Setter
	private String nome;

	@Size(max = 14)
	@NotNull
	@Getter @Setter
	private String cpf;

	@Size(max = 120)
	@Getter @Setter
	private String email;

	@Size(max = 20)
	@NotNull
	@Getter @Setter
	private String telefone;

	public Cliente(String nome, String cpf, String email, String telefone) {
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.telefone = telefone;
	}

}

