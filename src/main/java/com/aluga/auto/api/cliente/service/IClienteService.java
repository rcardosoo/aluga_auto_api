package com.aluga.auto.api.cliente.service;

import java.util.List;
import java.util.Optional;

import com.aluga.auto.api.cliente.model.Cliente;

public interface IClienteService {
	
	public Cliente cadastrarCliente(Cliente cliente);

	public Optional<Cliente> lerCliente(Long clienteId);

	public List<Cliente> listarClientes();
	
	public Cliente atualizarCliente(Long clienteId, Cliente novoCliente);
	
	public Cliente excluirCliente(Long clienteId);
}
