package com.aluga.auto.api.cliente.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aluga.auto.api.cliente.model.Cliente;
import com.aluga.auto.api.cliente.service.IClienteService;
import com.aluga.auto.api.exception.ResourceNotFoundException;

@RestController
@RequestMapping(value = "/api/clientes", produces = MediaType.APPLICATION_JSON_VALUE)
@SuppressWarnings("rawtypes")
public class ClienteController {

	@Autowired
	private IClienteService clienteService;
	
	@PostMapping()
	public ResponseEntity cadastrarCliente(@Valid @RequestBody Cliente cliente) {
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteService.cadastrarCliente(cliente));
	}
	
	@GetMapping(value = "/{clienteId}")
	public ResponseEntity lerCliente(@PathVariable("clienteId") Long clienteId) {
		Optional<Cliente> cliente = clienteService.lerCliente(clienteId);
		
		if (cliente.isPresent()) {
			return ResponseEntity.ok(cliente.get());
		}
		
		throw new ResourceNotFoundException(clienteId);
	}
	
	@GetMapping()
	public ResponseEntity<List<Cliente>> listarClientes() {
		return ResponseEntity.ok(clienteService.listarClientes());
	}
	
	@PutMapping(value = "/{clienteId}")
	public ResponseEntity atualizarCliente(@PathVariable("clienteId") Long clienteId, @Valid @RequestBody Cliente cliente) {
		Cliente atualizado = clienteService.atualizarCliente(clienteId, cliente);
		
		if (atualizado != null) {
			return ResponseEntity.ok(atualizado);
		}
		
		throw new ResourceNotFoundException(clienteId);
	}
	
	@DeleteMapping(value = "/{clienteId}")
	public ResponseEntity excluirCliente(@PathVariable("clienteId") Long clienteId) {
		Cliente deletado = clienteService.excluirCliente(clienteId);
		
		if (deletado != null) {
			return ResponseEntity.ok(deletado);
		}
		
		throw new ResourceNotFoundException(clienteId);
	}
}
