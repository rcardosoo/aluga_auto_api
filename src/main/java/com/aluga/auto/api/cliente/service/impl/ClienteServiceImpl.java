package com.aluga.auto.api.cliente.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aluga.auto.api.cliente.model.Cliente;
import com.aluga.auto.api.cliente.repository.IClienteRepository;
import com.aluga.auto.api.cliente.service.IClienteService;

@Service
public class ClienteServiceImpl implements IClienteService {
	
	@Autowired
	private IClienteRepository clienteRepository;
	
	@Override
	public Cliente cadastrarCliente(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	@Override
	public Optional<Cliente> lerCliente(Long clienteId) {
		return clienteRepository.findById(clienteId);
	}

	@Override
	public List<Cliente> listarClientes() {
		return clienteRepository.findAll();
	}

	@Override
	public Cliente atualizarCliente(Long clienteId, Cliente novoCliente) {
		return clienteRepository.findById(clienteId).map(cliente -> {
			novoCliente.setId(clienteId);
			return clienteRepository.save(novoCliente);
		}).orElse(null);
	}

	@Override
	public Cliente excluirCliente(Long clienteId) {
		return clienteRepository.findById(clienteId).map(cliente -> {
			clienteRepository.deleteById(clienteId);
			return cliente;
		}).orElse(null);
	}
	
}
