# Aluguel de Automóveis API

## Functionalidades

- [x] Cadastrar cliente
- [x] Atualizar cliente
- [x] Remover cliente 
- [x] Listar todos os cliente
- [x] Listar um cliente
- [x] Cadastrar veículo
- [x] Atualizar veículo
- [x] Remover veículo 
- [x] Listar todos os veículo
- [x] Listar um veículo
- [x] Alugar veículo
- [x] Entregar veículo
- [x] Reservar veículo
- [x] Cancelar reserva
- [x] Obter relatório gerencial  

## Como instalar

#### Dependências
* JDK 1.8
* PostgreSql: v9.6;

#### Instalação
1. Criar banco de dados vazio com o nome **aluga_auto**
2. Editar configurações no arquivo *src/main/resources/application.properties*
3. Abrir terminal no diretório raiz do projeto
4. Executar o comando:
    ```
    > ./mvnw spring-boot:run
    ```

5. A API deve estar disponível na porta **9000**.

## Endpoints

* **Cliente**

> Atributos de cliente:
```
{
	"nome": "Teste",
	"cpf": "111.222.333-45",
	"email": "teste.gmail.com",
	"telefone": "(84) 98887876"
}
```

| Tipo              | Endpoint                 | Corpo | Descrição        |
| :---- | -------------------: |:---------------:|---------------:|
| **GET**                 | /api/clientes  					| - |Lista todos os clientes cadastrados            |
| **GET**                 | /api/clientes/{ *cliente_id* }   	| - |Lista o cliente que possui o id informado            |
| **POST**                | /api/clientes   | Objeto Cliente 	| Cadastra o cliente enviado no corpo da requisição           |
| **PUT**                 | /api/clientes/{ *cliente_id* } 	| Objeto Cliente | Atualiza os dados do cliente que possui o id informado |
| **DELETE**              | /api/clientes/{ *cliente_id* }  	| - | Deleta o cliente que possui o id informado          |


* **Veículo**

> Atributos de veículo:
```
{
	"placa": "RRF7657",
	"modelo": "Gol",
	"marca": "Volkswagen",
	"cor": "Preto",
	"valorPorDia": 55.00
}
```

| Tipo              | Endpoint                 | Corpo | Descrição        |
| :---- | -------------------: |:---------------:|---------------:|
| **GET**                 | /api/veiculos  					| - |Lista todos os veiculos cadastrados            |
| **GET**                 | /api/veiculos/{ *veiculo_id* }   	| - |Lista o veiculo que possui o id informado            |
| **POST**                | /api/veiculos   | Objeto Veiculo 	| Cadastra o veiculo enviado no corpo da requisição           |
| **PUT**                 | /api/veiculos/{ *veiculo_id* } 	| Objeto Cliente | Atualiza os dados do veiculo que possui o id informado |
| **DELETE**              | /api/veiculos/{ *veiculo_id* }  	| - | Deleta o veiculo que possui o id informado          |

* **Locação**

| Tipo              | Endpoint                 | Corpo | Descrição        |
| :---- | -------------------: |:---------------:|---------------:|
| **POST**                 	| /clientes/{ *cliente_id* }/veiculos/{ *veiculo_id* }/locacoes  | { "dataEntrega": "dd/mm/yyy hh:mm:ss" } |Realiza a locação do veículo informado para o cliente informado            |
| **POST**                 	| /clientes/{ *cliente_id* }/veiculos/{ *veiculo_id* }/reservas  | { "dataEntrega": "dd/mm/yyy hh:mm:ss", "dataLocacao": "dd/mm/yyy hh:mm:ss"  } |Realiza o agendamento da locação do veículo informado para o cliente informado            |
| **PATCH**                 | /api/locacoes/{ *locacao_id* }   	| - |Finaliza a locação informada           |
| **PATCH**                 | /api/locacoes/{ *locacao_id* }/cancelamento 	| - | Cancela uma locação agendada |

* **Relatório**

| Tipo              | Endpoint                 | Corpo | Descrição        |
| :---- | -------------------: |:---------------:|---------------:|
| **GET**                 | /api/relatorios | - |Retorna o relatório gerencial do de todas as locações |
| **GET**                 | /api/relatorios?filtro=dia | - |Retorna o relatório gerencial do dia atual |
| **GET**                 | /api/relatorios?filtro=mes | - |Retorna o relatório gerencial do mês atual |
| **GET**                 | /api/relatorios?filtro=total | - |Retorna o relatório gerencial do de todas as locações |

